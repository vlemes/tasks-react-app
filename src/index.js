import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import injectTapEventPlugin from 'react-tap-event-plugin';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import 'normalize.css';
import 'styles/style.css';

import App from 'components/App';

import { addTaskList } from 'actions/TaskListsActions';
import store from './store';

if (store.getState().taskLists.size === 0) {
  store.dispatch(addTaskList('My List'));
}

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider>
      <App defaultTaskListId={store.getState().taskLists.first().id} />
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('root')
);
