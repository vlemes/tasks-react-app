import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import reducers from 'reducers';

const reducer = combineReducers(reducers);

function configureStore() {
  return createStore(
    reducer,
    applyMiddleware(thunk),
  );
}

export default configureStore();
