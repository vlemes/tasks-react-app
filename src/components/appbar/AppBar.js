import React, { PropTypes } from 'react';

import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui/svg-icons/navigation/menu';

import appBar from './AppBar.css';

const AppBar = (props, context) => {
  const {
    palette: {
      alternateTextColor,
    },
  } = context.muiTheme;

  return (
    <nav className={appBar.root}>
      <div className={appBar.icon}>
        <IconButton
          iconStyle={{ color: alternateTextColor }}
          onTouchTap={props.onIconTouch}
        >
          <MenuIcon />
        </IconButton>
      </div>
      <h2 className={appBar.title}>{props.title}</h2>
    </nav>
  );
};

AppBar.propTypes = {
  title: PropTypes.string,
  onIconTouch: PropTypes.func,
};

AppBar.contextTypes = {
  muiTheme: PropTypes.object.isRequired,
};

export default AppBar;
