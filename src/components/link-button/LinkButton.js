import React, { PropTypes } from 'react';
import cx from 'classnames';

import style from './LinkButton.css';

const LinkButton = ({ className, children, onTouchTap, ...other }) => (
  <a
    {...other}
    className={cx(className, style.linkButton)}
    onClick={e => e.preventDefault()}
    onTouchTap={e => {
      if (onTouchTap) {
        e.preventDefault();
        onTouchTap(e);
      }
    }}
  >
    {children}
  </a>
);

LinkButton.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  onTouchTap: PropTypes.func,
};

export default LinkButton;
