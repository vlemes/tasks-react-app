import React, { Component, PropTypes } from 'react';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';

class TaskListDialog extends Component {

  static propTypes = {
    open: PropTypes.bool,
    taskListName: PropTypes.string,
    onSubmit: PropTypes.func.isRequired,
    onRequestClose: PropTypes.func.isRequired,
  };

  state = {
    taskListName: this.props.taskListName,
    error: '',
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.open && !this.props.open) {
      this.setState({ taskListName: nextProps.taskListName });
    }
  }

  handleSubmit = () => {
    const {
      taskListName,
    } = this.state;

    if (taskListName === this.props.taskListName) {
      this.props.onRequestClose();
      return;
    }

    let error = '';

    if (!taskListName || !taskListName.trim()) {
      error = 'Name is required';
    }

    if (!error) {
      this.props.onSubmit(taskListName);
    }

    this.setState({ error });
  };

  render() {
    const {
      taskListName,
      onSubmit, // eslint-disable-line
      ...other
    } = this.props;
    return (
      <Dialog
        title={taskListName ? 'Rename Task List' : 'New Task List'}
        actions={[
          <FlatButton
            label="Cancel"
            secondary={true}
            onTouchTap={this.props.onRequestClose}
          />,
          <FlatButton
            label={taskListName ? 'Rename' : 'Add'}
            primary={true}
            onTouchTap={this.handleSubmit}
          />,
        ]}
        {...other}
      >
        <TextField
          floatingLabelText="Task List Name"
          hintText="My List"
          name="taskListName"
          value={this.state.taskListName}
          onChange={(event) => this.setState({ taskListName: event.target.value })}
          errorText={this.state.error}
        />
      </Dialog>
    );
  }
}

export default TaskListDialog;
