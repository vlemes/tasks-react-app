import React, { Component, PropTypes } from 'react';
import Immutable from 'immutable';

import { grey800 } from 'material-ui/styles/colors';

import { List, ListItem, makeSelectable } from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';

const SelectableList = makeSelectable(List);

const RENAME = 'RENAME';
const DELETE = 'DELETE';

class TaskLists extends Component {

  static propTypes = {
    taskLists: PropTypes.instanceOf(Immutable.List),
    selectedTaskListId: PropTypes.number,
    onSelectedItemChange: PropTypes.func,
    onRenameTaskList: PropTypes.func.isRequired,
    onDeleteTaskList: PropTypes.func.isRequired,
  };

  static defaultProps = {
    taskLists: new Immutable.List(),
  };

  handleIconButtonChange = (taskListId, event, value) => {
    switch (value) {
      case RENAME:
        this.props.onRenameTaskList(taskListId);
        break;
      case DELETE:
        this.props.onDeleteTaskList(taskListId);
        break;
      default:
        break;
    }
  };

  render() {
    const {
      taskLists,
      selectedTaskListId,
      onSelectedItemChange,
    } = this.props;

    const iconButtonElement = (
      <IconButton
        touch={true}
        tooltip="more"
        tooltipPosition="top-left"
      >
        <MoreVertIcon color={grey800} />
      </IconButton>
    );

    const menus = [
      <MenuItem key={0} value={RENAME}>Rename</MenuItem>,
      <MenuItem key={1} value={DELETE}>Delete</MenuItem>,
    ];

    return (
      <SelectableList
        value={selectedTaskListId}
        onChange={onSelectedItemChange}
      >
        <Subheader>Task lists</Subheader>
        {taskLists.map(taskList => (
          <ListItem
            key={taskList.id}
            value={taskList.id}
            primaryText={taskList.name}
            rightIconButton={
              <IconMenu
                iconButtonElement={iconButtonElement}
                onChange={(event, value) =>
                  this.handleIconButtonChange(taskList.id, event, value)
                }
              >
                {menus}
              </IconMenu>
            }
          />
        ))}
      </SelectableList>
    );
  }
}

export default TaskLists;
