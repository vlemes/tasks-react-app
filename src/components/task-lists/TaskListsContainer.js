import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { List } from 'immutable';

import { addTaskList, renameTaskList, deleteTaskList } from 'actions/TaskListsActions';

import RaisedButton from 'material-ui/RaisedButton';
import ContentAddIcon from 'material-ui/svg-icons/content/add';

import TaskListDialog from './TaskListDialog';
import TaskListDeleteDialog from './TaskListDeleteDialog';

import TaskLists from './TaskLists';

function mapStateToProps(state) {
  return {
    taskLists: state.taskLists,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onAddTaskList: bindActionCreators(addTaskList, dispatch),
    onRenameTaskList: bindActionCreators(renameTaskList, dispatch),
    onDeleteTaskList: bindActionCreators(deleteTaskList, dispatch),
  };
}

class TaskListsContainer extends Component {

  static propTypes = {
    taskLists: PropTypes.instanceOf(List),
    selectedTaskListId: PropTypes.number,
    onSelectTaskList: PropTypes.func,
    onAddTaskList: PropTypes.func.isRequired,
    onRenameTaskList: PropTypes.func.isRequired,
    onDeleteTaskList: PropTypes.func.isRequired,
  };

  static defaultProps = {
    taskLists: new List(),
  };

  state = {
    addNewTaskList: false,
    taskListIdToRename: undefined,
    taskListIdToDelete: undefined,
  };

  handleAddNewTaskList = (taskName) => {
    this.props.onAddTaskList(taskName);
    this.setState({ addNewTaskList: false });
  };

  handleRenameTaskList = (taskName) => {
    this.props.onRenameTaskList(this.state.taskListIdToRename, taskName);
    this.handleSetTaskListToRename(undefined);
  };

  handleDeleteTaskList = () => {
    this.props.onDeleteTaskList(this.state.taskListIdToDelete);
    this.handleSetTaskListToDelete(undefined);
  };

  handleSelectedItemChange = (event, id) => this.props.onSelectTaskList(id);

  handleSetTaskListToRename = taskListId => this.setState({ taskListIdToRename: taskListId });

  handleSetTaskListToDelete = taskListId => this.setState({ taskListIdToDelete: taskListId });

  render() {
    const {
      taskListIdToRename,
      taskListIdToDelete,
    } = this.state;

    const {
      taskLists,
      selectedTaskListId,
    } = this.props;

    const taskListIndex = taskLists.findIndex(
      l => l.id === (taskListIdToRename || taskListIdToDelete));

    const taskList = taskLists.get(taskListIndex);

    const dialogContentStyle = {
      width: 400,
      maxWidth: 'none',
    };
    const buttonStyle = {
      width: 130,
      display: 'block',
      margin: 'auto',
    };

    return (
      <div>
        <TaskLists
          taskLists={taskLists}
          selectedTaskListId={selectedTaskListId}
          onSelectedItemChange={this.handleSelectedItemChange}
          onRenameTaskList={this.handleSetTaskListToRename}
          onDeleteTaskList={this.handleSetTaskListToDelete}
        />
        <RaisedButton
          label="New List"
          secondary={true}
          icon={<ContentAddIcon />}
          style={buttonStyle}
          onTouchTap={() => this.setState({ addNewTaskList: true })}
        />
        <TaskListDialog
          taskListName=""
          open={this.state.addNewTaskList}
          contentStyle={dialogContentStyle}
          onRequestClose={() => this.setState({ addNewTaskList: false })}
          onSubmit={this.handleAddNewTaskList}
        />
        <TaskListDialog
          taskListName={taskList && taskList.name}
          open={!!this.state.taskListIdToRename}
          contentStyle={dialogContentStyle}
          onRequestClose={() => this.handleSetTaskListToRename(undefined)}
          onSubmit={this.handleRenameTaskList}
        />
        <TaskListDeleteDialog
          taskList={taskList}
          open={!!this.state.taskListIdToDelete}
          contentStyle={dialogContentStyle}
          onRequestClose={() => this.handleSetTaskListToDelete(undefined)}
          onSubmit={this.handleDeleteTaskList}
        />
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TaskListsContainer);
