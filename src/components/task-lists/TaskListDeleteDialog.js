import React, { PropTypes } from 'react';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

const TaskListDeleteDialog = (props) => {
  const {
    taskList,
    onRequestClose,
    onSubmit,
    ...other
  } = props;

  if (!taskList) {
    return null;
  }

  if (taskList.default) {
    return (
      <Dialog
        onRequestClose={onRequestClose}
        actions={[
          <FlatButton
            primary={true}
            label="OK"
            onTouchTap={onRequestClose}
          />,
        ]}
        {...other}
      >
        Your default list cannot be deleted.
      </Dialog>
    );
  }

  return (
    <Dialog
      onRequestClose={onRequestClose}
      actions={[
        <FlatButton
          secondary={true}
          label="Cancel"
          onTouchTap={onRequestClose}
        />,
        <FlatButton
          primary={true}
          label="OK"
          onTouchTap={onSubmit}
        />,
      ]}
      {...other}
    >
      Do you want to delete the list: <strong>{taskList.name}</strong>?
      The list will be permanently deleted.
    </Dialog>
  );
};

TaskListDeleteDialog.propTypes = {
  taskList: PropTypes.shape({
    name: PropTypes.string,
    default: PropTypes.bool,
  }),
  onSubmit: PropTypes.func,
  onRequestClose: PropTypes.func,
};

export default TaskListDeleteDialog;
