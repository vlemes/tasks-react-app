import React, { Component, PropTypes } from 'react';
import cx from 'classnames';

import ContentAddIcon from 'material-ui/svg-icons/content/add';
import Checkbox from 'material-ui/Checkbox';

import taskItemStyle from './TaskItem.css';

const getStyles = (props) => {
  const styles = {
    root: {},
    checkbox: {
      root: {
        width: 'auto',
      },
      rippleStyle: {
        top: -9,
        left: -9,
      },
    },
    iconStyle: {
      width: 18,
      height: 18,
      marginRight: 10,
    },
  };

  if (props.task.indent > 0) {
    styles.root.paddingLeft = 28 * props.task.indent;
  }

  return styles;
};

class TaskItem extends Component {

  static propTypes = {
    task: PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      description: PropTypes.string,
      dueDate: PropTypes.date,
      completed: PropTypes.bool,
      indent: PropTypes.number,
    }),
    onCheck: PropTypes.func,
    onChange: PropTypes.func,
    onKeyDown: PropTypes.func,
    onKeyUp: PropTypes.func,
    focusOnMount: PropTypes.bool,
    last: PropTypes.bool,
    style: PropTypes.object,
  };

  static defaultProps = {
    task: {},
  };

  state = {
    isMouseOverItem: false,
    isFieldFocused: false,
  };

  componentDidMount() {
    if (this.props.focusOnMount) {
      this.field.focus();
    }
    if (this.props.task.title) {
      this.field.setSelectionRange(0);
    }
  }

  handleCheck = (event, isInputChecked) => {
    if (this.props.onCheck) {
      this.props.onCheck(
        this.props.task,
        isInputChecked,
        event
      );
    }
  };

  handleFieldFocus = () => {
    this.setState({
      isFieldFocused: true,
    });
  };

  handleFieldChange = (event) => {
    if (this.props.onChange) {
      this.props.onChange({
        ...this.props.task,
        title: event.target.value,
      }, event);
    }
  };

  handleFieldBlur = () => {
    this.setState({
      isFieldFocused: false,
    });
  };

  handleMouseOverItem = () => {
    this.setState({
      isMouseOverItem: true,
    });
  };

  handleMouseOutItem = () => {
    this.setState({
      isMouseOverItem: false,
    });
  };

  render() {
    const {
      task,
      last,
      onKeyDown,
      onKeyUp,
      style,
    } = this.props;

    const styles = getStyles(this.props, this.state);
    return (
      <div
        className={cx(
          taskItemStyle.root,
          { [taskItemStyle.itemFocused]: this.state.isFieldFocused },
        )}
        onMouseOver={this.handleMouseOverItem}
        onMouseOut={this.handleMouseOutItem}
        style={{ ...style, ...styles.root }}
      >
        {last ?
          <ContentAddIcon style={styles.iconStyle} />
        :
          <Checkbox
            className={taskItemStyle.checkbox}
            checked={task.completed}
            iconStyle={styles.iconStyle}
            onCheck={this.handleCheck}
            rippleStyle={styles.checkbox.rippleStyle}
            style={styles.checkbox.root}
          />
        }
        <input
          ref={field => { this.field = field; }}
          className={cx('field',
            taskItemStyle.field,
            { [taskItemStyle.completed]: task.completed },
          )}
          onBlur={this.handleFieldBlur}
          onChange={this.handleFieldChange}
          onFocus={this.handleFieldFocus}
          onKeyDown={onKeyDown}
          onKeyUp={onKeyUp}
          type="text"
          placeholder={last ? 'List item' : ''}
          value={task.title}
        />
      </div>
    );
  }
}

export default TaskItem;
