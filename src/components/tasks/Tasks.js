import React, { Component, PropTypes } from 'react';
import cx from 'classnames';
import { List } from 'immutable';

import Paper from 'material-ui/Paper';

import TaskItem from 'components/task-item/TaskItem';

import tasksStyle from './Tasks.css';

class Tasks extends Component {

  static propTypes = {
    className: PropTypes.string,
    tasks: PropTypes.instanceOf(List),
    title: PropTypes.string,
    onItemCheck: PropTypes.func.isRequired,
    onItemChange: PropTypes.func.isRequired,
    onItemKeyDown: PropTypes.func.isRequired,
  };

  static defaultProps = {
    tasks: new List(),
  };

  focusItem = index => {
    if (index >= 0 && index < this.props.tasks.size) {
      this.list.childNodes[index].getElementsByClassName('field')[0].focus();
    }
  };

  render() {
    const {
      className,
      tasks,
      title,
      onItemCheck,
      onItemChange,
      onItemKeyDown,
      ...other
    } = this.props;

    if (tasks.size === 0) {
      return null;
    }

    const items = tasks.map((t, i) => {
      const last = tasks.size - 1 === i;
      return (
        <TaskItem
          key={t.id}
          task={t}
          onCheck={onItemCheck}
          onChange={(task, event) => onItemChange(task, i, event)}
          onKeyDown={event => onItemKeyDown(t, i, event)}
          focusOnMount={!last}
          last={last}
        />
      );
    });

    return (
      <Paper className={cx(className, tasksStyle.root)} {...other}>
        <div className={cx(tasksStyle.title)}>{title}</div>
        <div ref={list => { this.list = list; }}>
          {items}
        </div>
      </Paper>
    );
  }
}

export default Tasks;
