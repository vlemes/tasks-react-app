import React, { Component } from 'react';
import cx from 'classnames';
import {
  Toolbar,
  ToolbarGroup,
  ToolbarSeparator,
  ToolbarTitle,
} from 'material-ui/Toolbar';
import FlatButton from 'material-ui/FlatButton';
import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';
import ArrowUpward from 'material-ui/svg-icons/navigation/arrow-upward';
import ArrowDownward from 'material-ui/svg-icons/navigation/arrow-downward';
import ContentAddIcon from 'material-ui/svg-icons/content/add';
import EditorIndentIcon from 'material-ui/svg-icons/editor/format-indent-increase';
import EditorUnindentIcon from 'material-ui/svg-icons/editor/format-indent-decrease';
import DeleteIcon from 'material-ui/svg-icons/action/delete';

import style from './TasksToolbar.css';

class TasksToolbar extends Component {
  render() {
    const {
      disabledClearCompleted,
      onClearCompletedTap,
    } = this.props;
    return (
      <Paper className={cx(style.root)}>
        <Toolbar>
          <ToolbarGroup firstChild={true}>
            <FlatButton
              label="Clear Completed"
              disabled={disabledClearCompleted}
              onTouchTap={onClearCompletedTap}
            />

            <ToolbarSeparator />

            <FlatButton
              label="New task"
              icon={<ContentAddIcon />}
            />

            <ToolbarSeparator />

            <IconButton tooltip="Indent">
              <EditorIndentIcon />
            </IconButton>
            <IconButton tooltip="Unindent">
              <EditorUnindentIcon />
            </IconButton>

            <ToolbarSeparator />

            <IconButton tooltip="Move up">
              <ArrowUpward />
            </IconButton>
            <IconButton tooltip="Move down">
              <ArrowDownward />
            </IconButton>

            <ToolbarSeparator />
            <IconButton tooltip="Delete">
              <DeleteIcon />
            </IconButton>

          </ToolbarGroup>
        </Toolbar>
      </Paper>
    );
  }
}

export default TasksToolbar;
