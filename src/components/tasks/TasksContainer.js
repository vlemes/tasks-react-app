import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { List } from 'immutable';

import {
  addTask,
  deleteTask,
  editTask,
  increaseIndent,
  decreaseIndent,
  completeTask,
  incompleteTask,
  clearCompletedTasks,
  moveUpTask,
  moveDownTask,
} from 'actions/TaskActions';

import Tasks from './Tasks';

function mapStateToProps(state, props) {
  return {
    tasks: state.tasks.get(`${props.taskListId}`),
    taskList: state.taskLists.find(l => l.id === props.taskListId),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onAddTask: bindActionCreators(addTask, dispatch),
    onDeleteTask: bindActionCreators(deleteTask, dispatch),
    onEditTask: bindActionCreators(editTask, dispatch),
    onIncreaseIndent: bindActionCreators(increaseIndent, dispatch),
    onDecreaseIndent: bindActionCreators(decreaseIndent, dispatch),
    onTaskComplete: bindActionCreators(completeTask, dispatch),
    onTaskIncomplete: bindActionCreators(incompleteTask, dispatch),
    onClearCompletedTasks: bindActionCreators(clearCompletedTasks, dispatch),
    onTaskMoveUp: bindActionCreators(moveUpTask, dispatch),
    onTaskMoveDown: bindActionCreators(moveDownTask, dispatch),
  };
}

class TasksContainer extends Component {

  static propTypes = {
    taskListId: PropTypes.number,
    tasks: PropTypes.instanceOf(List),
    taskList: PropTypes.shape({
      name: PropTypes.string,
    }),
    onAddTask: PropTypes.func.isRequired,
    onEditTask: PropTypes.func.isRequired,
    onDeleteTask: PropTypes.func.isRequired,
    onIncreaseIndent: PropTypes.func.isRequired,
    onDecreaseIndent: PropTypes.func.isRequired,
    onTaskComplete: PropTypes.func.isRequired,
    onTaskIncomplete: PropTypes.func.isRequired,
    onClearCompletedTasks: PropTypes.func.isRequired,
    onTaskMoveUp: PropTypes.func.isRequired,
    onTaskMoveDown: PropTypes.func.isRequired,
  };

  static defaultProps = {
    tasks: new List(),
  };

  handleBackspace = (task, index, event) => {
    const {
      tasks,
      taskListId,
      onDeleteTask,
      onEditTask,
    } = this.props;

    // first item
    if (index === 0) {
      return;
    }

    // last item
    if (index === tasks.size - 1) {
      event.preventDefault();
      this.list.focusItem(index - 1);
      return;
    }

    // cursor at beginning
    if (event.target.selectionStart === 0) {
      if (task.title) {
        // edit
        const prevTask = tasks.get(index - 1);
        onEditTask({
          ...prevTask,
          title: `${prevTask.title}${task.title}`,
        }, taskListId);
      }
      // delete
      onDeleteTask(task, taskListId);
      event.preventDefault();
      this.list.focusItem(index - 1);
    }
  };

  handleCheck = (task, isComplete) => (
    isComplete ? this.props.onTaskComplete(task, this.props.taskListId)
      : this.props.onTaskIncomplete(task, this.props.taskListId)
  );

  handleChange = (task, index) => {
    const {
      tasks,
      taskListId,
      onAddTask,
      onEditTask,
    } = this.props;

    // last item
    if (index === tasks.size - 1) {
      onAddTask(undefined, undefined, taskListId);
    }
    onEditTask(task, taskListId);
  };

  handleEnterPress = (task, index, event) => {
    const {
      tasks,
      taskListId,
      onAddTask,
      onEditTask,
      onDecreaseIndent,
    } = this.props;

    // last item
    if (index === tasks.size - 1) {
      return;
    }

    let title = '';

    // cursor not in the end
    if (event.target.selectionStart <= task.title.length - 1) {
      onEditTask({
        ...task,
        title: task.title.slice(0, event.target.selectionStart),
      }, taskListId);
      title = task.title.slice(event.target.selectionStart);
    } else if (index === tasks.size - 2) { // last but one
      if (task.indent === 0) {
        this.list.focusItem(index + 1);
        return;
      }
      if (!task.title) { // empty indented item
        onDecreaseIndent(task, taskListId);
        return;
      }
    }

    onAddTask(index + 1, { title }, taskListId);
  };

  handleTab = (task, index, event) => {
    event.preventDefault();
    if (event.shiftKey) {
      this.props.onDecreaseIndent(task, this.props.taskListId);
    } else {
      this.props.onIncreaseIndent(task, this.props.taskListId);
    }
  };

  handleArrowUp = (task, index, event) => {
    if (event.metaKey) {
      this.props.onTaskMoveUp(task, this.props.taskListId);
      return;
    }

    this.list.focusItem(index - 1);
  };

  handleArrowDow = (task, index, event) => {
    if (event.metaKey) {
      this.props.onTaskMoveDown(task, this.props.taskListId);
      return;
    }

    this.list.focusItem(index + 1);
  };

  handleKeyDown = (task, index, event) => {
    switch (event.key) {
      case 'Enter':
        this.handleEnterPress(task, index, event);
        break;
      case 'Backspace':
        this.handleBackspace(task, index, event);
        break;
      case 'Tab':
        this.handleTab(task, index, event);
        break;
      case 'ArrowUp':
        this.handleArrowUp(task, index, event);
        break;
      case 'ArrowDown':
        this.handleArrowDow(task, index, event);
        break;
      default:
        break;
    }
  };

  handleClearCompletedTasks = () => this.props.onClearCompletedTasks(this.props.taskListId);

  render() {
    const {
      tasks,
      taskList,
      taskListId, // eslint-disable-line
      onAddTask, // eslint-disable-line
      onEditTask, // eslint-disable-line
      onDeleteTask, // eslint-disable-line
      onIncreaseIndent, // eslint-disable-line
      onDecreaseIndent, // eslint-disable-line
      onTaskComplete, // eslint-disable-line
      onTaskIncomplete, // eslint-disable-line
      onClearCompletedTasks, // eslint-disable-line
      onTaskMoveUp, // eslint-disable-line
      onTaskMoveDown, // eslint-disable-line
      ...other
    } = this.props;

    return (
      <div {...other}>
        <Tasks
          ref={list => { this.list = list; }}
          tasks={tasks}
          title={taskList && taskList.name}
          onItemCheck={this.handleCheck}
          onItemChange={this.handleChange}
          onItemKeyDown={this.handleKeyDown}
        />
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TasksContainer);
