import React, { Component, PropTypes } from 'react';
import cx from 'classnames';

import AppBar from 'components/appbar/AppBar';
import Drawer from 'material-ui/Drawer';

import TaskListsContainer from 'components/task-lists/TaskListsContainer';
import TasksContainer from 'components/tasks/TasksContainer';

import appStyle from 'components/App.css';

class App extends Component {

  static propTypes = {
    defaultTaskListId: PropTypes.number,
  };

  state = {
    open: true,
    selectedTaskListId: this.props.defaultTaskListId,
  };

  handleDrawerToggle = () => this.setState({ open: !this.state.open });

  handleSelectTaskList = id => this.setState({ selectedTaskListId: id });

  render() {
    const appBar = (
      <AppBar
        title="Tasks"
        onIconTouch={this.handleDrawerToggle}
      />
    );

    return (
      <div>
        {appBar}
        <TasksContainer
          taskListId={this.state.selectedTaskListId}
          className={cx(appStyle.list, { [appStyle.drawerOpened]: this.state.open })}
        />
        <Drawer open={this.state.open}>
          {appBar}
          <TaskListsContainer
            selectedTaskListId={this.state.selectedTaskListId}
            onSelectTaskList={this.handleSelectTaskList}
          />
        </Drawer>
      </div>
    );
  }
}

export default App;
