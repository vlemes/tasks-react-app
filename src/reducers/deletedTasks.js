import { DELETE_TASK } from 'actions/TaskActions';

const deletedTasks = (state = [], action) => {
  switch (action.type) {
    case DELETE_TASK:
      return [
        ...state,
        action.task,
      ];
    default:
      return state;
  }
};

export default deletedTasks;
