import { List, Map } from 'immutable';
import {
  ADD_TASK,
  EDIT_TASK,
  DELETE_TASK,
  INCREASE_INDENT,
  DECREASE_INDENT,
  COMPLETE_TASK,
  INCOMPLETE_TASK,
  CLEAR_COMPLETED_TASKS,
  MOVE_UP_TASK,
  MOVE_DOWN_TASK,
} from 'actions/TaskActions';
import {
  DELETE_TASK_LIST,
} from 'actions/TaskListsActions';

let taskId;

const intialTaskState = {
  id: undefined,
  title: '',
  description: '',
  dueDate: undefined,
  completed: false,
  indent: undefined,
};

const task = (state = intialTaskState, action) => {
  switch (action.type) {
    case ADD_TASK:
      return {
        ...state,
        ...action.task,
        id: taskId++,
      };

    case EDIT_TASK:
      return {
        ...state,
        ...action.task,
      };

    case INCREASE_INDENT:
      return {
        ...state,
        indent: state.indent + 1,
      };

    case DECREASE_INDENT:
      return {
        ...state,
        indent: state.indent - 1,
      };

    case COMPLETE_TASK: {
      if (!state.completed) {
        return {
          ...state,
          completed: true,
        };
      }
      return state;
    }

    case INCOMPLETE_TASK: {
      if (state.completed) {
        return {
          ...state,
          completed: false,
        };
      }
      return state;
    }

    default:
      return state;
  }
};

const tasks = (state = new List(), action) => {
  switch (action.type) {
    case ADD_TASK: {
      // init id
      if (!taskId) {
        taskId = state.size + 1;
      }
      const prevTask = state.get(action.index - 1);
      const newTask = task(undefined, {
        type: ADD_TASK,
        task: {
          ...action.task,
          indent: prevTask ? prevTask.indent : 0, // initial indent equal to previous
        },
      });
      // insert at index or in the end when not defined
      const newState = state.insert(action.index || state.size, newTask);
      // new task may incomplete parent tasks
      return tasks(newState, { type: INCOMPLETE_TASK, task: newTask });
    }

    case CLEAR_COMPLETED_TASKS:
      return state.filter(t => !t.completed);

    default:
      break;
  }

  if (!action.task) {
    return state;
  }

  const index = state.findIndex(t => t.id === action.task.id);

  // whether task not foudn
  if (index < 0) {
    return state;
  }

  let parentIndex = index;
  // whether task has parent
  if (action.task.indent > 0) {
    state.forEach((t, i) => {
      if (t.indent === 0) {
        parentIndex = i;
      }
      return t.id !== action.task.id;
    });
  }

  let lastIndex = index;
  // whether task has children
  if (index < state.size - 1 // not last one
    && state.get(index + 1).indent > action.task.indent // next is children
  ) {
    state.forEach((t, i) => {
      if (i > index) {
        if (t.indent <= action.task.indent) {
          return false;
        }
        lastIndex = i;
      }
      return true;
    });
  }

  switch (action.type) {

    case EDIT_TASK:
      return state.set(index, task(state.get(index), action));

    case DELETE_TASK:
      return state.delete(index);

    case INCREASE_INDENT:
    case DECREASE_INDENT: {
      // dont indent first and last
      if (index === 0 || index === state.size - 1) {
        return state;
      }
      // dont increase max indent
      if (action.type === INCREASE_INDENT
        && action.task.indent > state.get(index - 1).indent) {
        return state;
      }
      // dont decrease min indent
      if (action.type === DECREASE_INDENT
        && action.task.indent === 0) {
        return state;
      }
      // indent task and its children
      const newState = state.map((t, i) => {
        if (i >= index && i <= lastIndex) {
          return task(t, action);
        }
        return t;
      });
      // increase indent may incomplete parent tasks
      if (action.type === INCREASE_INDENT) {
        const t = newState.get(index);
        // when task not complete and not root
        if (!t.completed && t.indent > 0) {
          return tasks(newState, {
            type: INCOMPLETE_TASK,
            task: newState.get(index - 1), // get parent task
          });
        }
      }
      return newState;
    }

    case COMPLETE_TASK:
    case INCOMPLETE_TASK:
      // complete or imcomplete:
      return state.map((t, i) => {
        if (i === index // itself
          || (i > index && i <= lastIndex
            && t.indent > action.task.indent) // all its children
          || (action.type === INCOMPLETE_TASK
            && i >= parentIndex && i < index
            && t.indent < action.task.indent) // all its children and parents
        ) {
          return task(t, action);
        }
        return t;
      });

    case MOVE_UP_TASK: {
      // not first and not last
      if (index === 0 || index === state.size - 1) {
        return state;
      }

      // remove task and its children
      const newState = state.splice(index, (lastIndex - index) + 1);

      // task and its children to move
      const toMove = state.slice(index, lastIndex + 1);

      let newIndex = -1;
      for (let i = index - 1; i >= 0 && newIndex === -1; i--) {
        const t = newState.get(i);
        if (t.indent <= action.task.indent) {
          newIndex = i;
        }
      }

      const indentDiff = action.task.indent - newState.get(newIndex).indent;

      return newState.splice(newIndex, 0, ...toMove.map(t => {
        if (indentDiff > 0) {
          return { ...t, indent: t.indent - indentDiff };
        }
        return t;
      }));
    }

    case MOVE_DOWN_TASK: {
      // not last but one and not last
      if (index >= state.size - 2) {
        return state;
      }

      return tasks(state, {
        type: MOVE_UP_TASK,
        task: state.get(lastIndex + 1),
        taskListId: action.taskListId,
      });
    }

    default:
      return state;
  }
};

const tasksByTaskListId = (state = new Map(), action) => {
  switch (action.type) {
    case DELETE_TASK_LIST:
      return state.remove(`${action.id}`);

    default: {
      if (!action.taskListId) {
        return state;
      }

      const tasksByListId = state.get(`${action.taskListId}`);
      const newTasksByListId = tasks(tasksByListId, action);

      if (tasksByListId === newTasksByListId) {
        return state;
      }

      return state.set(`${action.taskListId}`, newTasksByListId);
    }
  }
};

export default tasksByTaskListId;
