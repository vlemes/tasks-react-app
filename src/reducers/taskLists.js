import { List } from 'immutable';
import {
  ADD_TASK_LIST,
  RENAME_TASK_LIST,
  DELETE_TASK_LIST,
} from 'actions/TaskListsActions';

let taskListId;

const initialTaskListState = {
  id: undefined,
  name: '',
  default: false,
};

const taskList = (state = initialTaskListState, action) => {
  switch (action.type) {
    case ADD_TASK_LIST:
      return {
        ...state,
        name: action.name,
        id: taskListId++,
        default: action.default,
      };

    case RENAME_TASK_LIST:
      return {
        ...state,
        name: action.name,
      };

    default:
      return state;
  }
};

const taskLists = (state = new List(), action) => {
  if (action.type === ADD_TASK_LIST) {
    // init id
    if (!taskListId) {
      taskListId = state.size + 1;
    }
    return state.push(taskList(undefined, {
      ...action,
      default: state.size === 0,
    }));
  }

  if (!action.id) {
    return state;
  }

  const index = state.findIndex(l => l.id === action.id);

  if (index < 0) {
    return state;
  }

  switch (action.type) {

    case RENAME_TASK_LIST:
      return state.set(index, taskList(state.get(index), action));

    case DELETE_TASK_LIST:
      return state.delete(index);

    default:
      return state;
  }
};

export default taskLists;
