import taskLists from './taskLists';
import tasks from './tasks';
import deletedTasks from './deletedTasks';

export default { taskLists, tasks, deletedTasks };
