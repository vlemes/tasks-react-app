import { addTask } from 'actions/TaskActions';

export const ADD_TASK_LIST = 'ADD_TASK_LIST';
export const RENAME_TASK_LIST = 'RENAME_TASK_LIST';
export const DELETE_TASK_LIST = 'DELETE_TASK_LIST';

function addNewTaskList(name) {
  return {
    type: ADD_TASK_LIST,
    name,
  };
}

export const addTaskList = name => (dispatch, getState) => {
  dispatch(addNewTaskList(name));
  dispatch(addTask(undefined, undefined, getState().taskLists.last().id));
};

export const renameTaskList = (id, name) => ({
  type: RENAME_TASK_LIST,
  id,
  name,
});

export const deleteTaskList = id => ({
  type: DELETE_TASK_LIST,
  id,
});
