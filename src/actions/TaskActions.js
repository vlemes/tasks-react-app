export const ADD_TASK = 'ADD_TASK';
export const EDIT_TASK = 'EDIT_TASK';
export const DELETE_TASK = 'DELETE_TASK';
export const INCREASE_INDENT = 'INCREASE_INDENT';
export const DECREASE_INDENT = 'DECREASE_INDENT';
export const COMPLETE_TASK = 'COMPLETE_TASK';
export const INCOMPLETE_TASK = 'INCOMPLETE_TASK';
export const CLEAR_COMPLETED_TASKS = 'CLEAR_COMPLETED_TASKS';
export const MOVE_UP_TASK = 'MOVE_UP_TASK';
export const MOVE_DOWN_TASK = 'MOVE_DOWN_TASK';

export const addTask = (index, task, taskListId) => ({
  type: ADD_TASK,
  index,
  task,
  taskListId,
});

export const deleteTask = (task, taskListId) => ({
  type: DELETE_TASK,
  task,
  taskListId,
});

export const editTask = (task, taskListId) => ({
  type: EDIT_TASK,
  task,
  taskListId,
});

export const increaseIndent = (task, taskListId) => ({
  type: INCREASE_INDENT,
  task,
  taskListId,
});

export const decreaseIndent = (task, taskListId) => ({
  type: DECREASE_INDENT,
  task,
  taskListId,
});

export const completeTask = (task, taskListId) => ({
  type: COMPLETE_TASK,
  task,
  taskListId,
});

export const incompleteTask = (task, taskListId) => ({
  type: INCOMPLETE_TASK,
  task,
  taskListId,
});

export const clearCompletedTasks = taskListId => ({
  type: CLEAR_COMPLETED_TASKS,
  taskListId,
});

export const moveUpTask = (task, taskListId) => ({
  type: MOVE_UP_TASK,
  task,
  taskListId,
});

export const moveDownTask = (task, taskListId) => ({
  type: MOVE_DOWN_TASK,
  task,
  taskListId,
});
