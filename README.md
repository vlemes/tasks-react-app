# Tasks React Web App
> A Flux React web application

## Installation
```sh
npm install
```

## Run
```sh
npm start
```
Then open http://localhost:8080.

## Lint
```sh
npm run lint
```

## Test
```sh
npm test
```
